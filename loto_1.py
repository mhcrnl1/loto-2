#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

class Loto:
    ''' Clasa loterie'''
    interval_numar_min = 0
    interval_munar_max = 80
    numere_generate = 6
    numere_loto = []

    def __init__(self,interval_numar_min=1, interval_munar_max=80, numere_generate=6):
        print("Salut din programul de generare numere aleatorii pentru loterie!")
        #self.interval_numar_min = interval_numar_min
        #self.interval_numar_max = interval_numar_max
        #self.numere_generate = numere_generate
        self.interval_numar_min = int(input("Introduceti numarul minim al intervalului:"))
        self.interval_numar_max = int(input("Introduceti numarul maxim al intervalului:"))
        self.numere_generate = int(input("Introduceti numarul de numere generate:"))
        numere_loto = []

        #generator(self, self.interval_numar_min, self.interval_numar_max, self.numere_generate):
        while True:
            if len(numere_loto) == self.numere_generate :
                break
            else :
                generator_numere = random.randint(self.interval_numar_min, self.interval_numar_max)
                if self.numere_generate not in numere_loto:
                    numere_loto.append(generator_numere)

        print(numere_loto)     

loto = Loto()
#loto.greet()
#loto.generator()


